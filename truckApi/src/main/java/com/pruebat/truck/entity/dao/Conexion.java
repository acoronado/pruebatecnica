package com.pruebat.truck.entity.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

	String driver = "com.mysql.cj.jdbc.Driver";
	String database = "bdtrucks";
	String hostname = "localhost";
	String url = "jdbc:mysql://localhost/bdtrucks?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	String username = "root";
	String password = "Admin";

	public Connection conetarMySQL() {

		Connection con = null;

		try {

			Class.forName(driver);
			con = (Connection) DriverManager.getConnection(url, username, password);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

}
