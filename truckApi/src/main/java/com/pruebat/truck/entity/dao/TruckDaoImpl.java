package com.pruebat.truck.entity.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.pruebat.truck.entity.interfaces.ITruckDao;
import com.pruebat.truck.entity.models.Truck;


public class TruckDaoImpl implements ITruckDao{

	public String insertTruckBd(List<Truck> trucks) {
		Conexion sql = new Conexion();
		Connection con = sql.conetarMySQL();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String query = "INSERT INTO trucks (idtruck, brand, model, creationdate, description) VALUES(?,?,?,?,?)";

		try {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);

			for (int i = 0; i < trucks.size(); i++) {
				ps.setLong(1, trucks.get(i).getIdtruck());
				ps.setString(2, trucks.get(i).getBrand());
				ps.setString(3, trucks.get(i).getModel());
				ps.setString(4, formatter.format(trucks.get(i).getCreationdate()));
				ps.setString(5, trucks.get(i).getDescription());

				ps.executeUpdate();
			}
			ps.close();
			con.close();
			return "ok";
		} catch (SQLException e) {
			return "Error al Insertar";
		}
	}

}
