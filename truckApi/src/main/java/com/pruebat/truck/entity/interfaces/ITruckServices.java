package com.pruebat.truck.entity.interfaces;

import java.util.List;

import com.pruebat.truck.entity.models.Truck;

public interface ITruckServices {

	public String truckFileInsert(String email);

	public String trucksendMail(String email, int totalInser, int fallidos);

	public List<Truck> importCSV(String scr);

}
