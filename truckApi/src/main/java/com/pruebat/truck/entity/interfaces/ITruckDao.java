package com.pruebat.truck.entity.interfaces;

import java.util.List;

import com.pruebat.truck.entity.models.Truck;

public interface ITruckDao {
	
	public String insertTruckBd(List<Truck> trucks);

}
