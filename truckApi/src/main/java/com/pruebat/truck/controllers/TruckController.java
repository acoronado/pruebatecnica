package com.pruebat.truck.controllers;

import java.io.File;
import java.io.FileOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pruebat.truck.entity.interfaces.ITruckServices;

@RestController
public class TruckController {

	@Autowired
	private ITruckServices truckService;

	@PostMapping("/upload")
	public String FileUpload(@RequestParam("file") MultipartFile file, @RequestParam("email") String email) {
		try {
		File convertFile = new File("../truckApi/tmp/" + "cargue.csv");

		convertFile.createNewFile();

		try (FileOutputStream fout = new FileOutputStream(convertFile)) {
			fout.write(file.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (convertFile != null) {
			truckService.truckFileInsert(email);

		}
		}catch (Exception e) {
			return "error";
		}
		return "ok";

	}
}
