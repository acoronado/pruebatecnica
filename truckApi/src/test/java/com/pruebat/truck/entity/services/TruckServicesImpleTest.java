package com.pruebat.truck.entity.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.pruebat.truck.entity.models.Truck;

@RunWith(MockitoJUnitRunner.class)
public class TruckServicesImpleTest {

	@InjectMocks
	TruckServicesImple truckServicesImple;
	
	@Mock
	TruckServicesImple truckServicesImpl;
	
	@Mock
	SendMailServices sendMailService;
	
	
	@Test
	public void testImportCSV() {
		 when(truckServicesImpl.importCSV("../truckApi/tmp/cargue.csv")).thenReturn(new ArrayList<Truck>());
		 String src="../truckApi/tmp/cargue.csv";
		 List<Truck> list = truckServicesImpl.importCSV(src);
		 List<Truck> list2= new ArrayList<Truck>();
		 assertEquals(list2, list);
	}

	@Test
	public void testTruckFileInsert() {
		 when(truckServicesImpl.truckFileInsert("correo")).thenReturn("ok");
		 String result = truckServicesImple.truckFileInsert("p");
		 assertEquals("ok", result);
	}

	@Test
	public void testTrucksendMail() {
		when(sendMailService.sendMail("p1","p2","p3","p4")).thenReturn("send");
		String result = truckServicesImple.trucksendMail("ajcramirez@gmail.com",4,6);
		assertEquals("send", result);
	}

}
