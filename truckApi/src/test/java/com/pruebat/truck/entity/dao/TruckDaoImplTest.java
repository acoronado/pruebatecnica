package com.pruebat.truck.entity.dao;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.pruebat.truck.entity.models.Truck;

@RunWith(MockitoJUnitRunner.class)
public class TruckDaoImplTest {
	
	@Mock
	TruckDaoImpl truckDaoImplmock;
	
	@InjectMocks
	TruckDaoImpl truckDaoImpl;

	
	

	@Test
	public void testInsertTruckBd() {
		
		List<Truck> trucks= new ArrayList<Truck>();
		when(truckDaoImplmock.insertTruckBd(trucks)).thenReturn("ok");
		
		 String resultado = truckDaoImpl.insertTruckBd(trucks);
		 
		 assertEquals("ok", resultado);
		
	}

}
